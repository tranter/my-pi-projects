import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(7, GPIO.OUT)
for x in range(0,99) :
    GPIO.output(7, True)
    time.sleep(0.05)
    GPIO.output(7, False)
    time.sleep(0.05)
